#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "graph.h"

#define valores_iniciais 30
#define MAX_ELEM_MATRIZ 81

typedef struct Valores{
	int valor;
	int linha;
	int coluna;
}Valores;

typedef struct{
	int quadrante;
	struct Valores valores[9];
}Verificacao;

//Matriz que guarda os três primeiros elementos de cada bloco 3x3
int primeiros_tres_elementos[3][9];

void flush_in(){ 
	
	int ch;
	while((ch = fgetc(stdin)) != EOF && ch != '\n' ){} 
}

void printa_matriz(int matriz[9][9]){
	
	int j,k, contador = 0;
	
	for(j=0;j<9;j++){
		for(k=0;k<9;k++){
			if (matriz[j][k] != 0)
				contador++;
			printf("%d\t", matriz[j][k]);
		}
		printf("\n");
	}
}

//Função que inicializará a matriz que conterão os primeiros 3 elementos de cada bloco
void inicializa_matriz_de_iniciais(){
	
	int i, j, contador=0;
	
	for(i=0;i<9;i+=3){
		for(j=0;j<3;j++){
			primeiros_tres_elementos[i/3][contador] = ((i*9)+(j*9)+0);
			contador+=1;
			primeiros_tres_elementos[i/3][contador] = ((i*9)+(j*9)+1);
			contador+=1;
			primeiros_tres_elementos[i/3][contador] = ((i*9)+(j*9)+2);
			contador+=1;
		}
		contador = 0;
	}
	
// 	for(i=0;i<3;i++){
// 		for(j=0;j<9;j++){
// 			printf("%d ", primeiros_tres_elementos[i][j]);
// 		}
// 		printf("\n");
// 	}
}

//Checa se o índice fornecido está no grupo 9x9
bool verifica_indice_grupo(int *group, int size, int index){
	
	int i = 0;
	for (i = 0; i < size; i++)
		if (group[i] == index)
			return true;
	
	return false;
}

// link a vertice at index with every other vertice in the group
void linka_vertice_com_grupo(int *group, int size, int index, struct vertice **vertices){
	
	struct vertice *to_link = vertices[index];

	int i = 0;
	for (i = 0; i < size; i++)
		if (group[i] != index)
			adiciona_aresta(to_link, vertices[group[i]]);
}

//Linka grupos 3x3 na lista de vértices
void linka_grupos_3(struct vertice **vertices){
	
	// link 3x3 groups from initial data
	int i = 0;
	int j = 0;
	int grupo_atual[9];

	for (i = 0; i < 9; i++) {
		int factor = 0;
		if (i > 2 && i < 6)
			factor = 1;
		else if (i > 5)
			factor = 2;

		for (j = 0; j < 9; j++)
			grupo_atual[j] = primeiros_tres_elementos[i % 3][j] + (factor * 3); 

		//Procura pelos índices dos vértices e linka-os em um grupo junto
		for (j = 0; j < 81; j++)
			if (verifica_indice_grupo(grupo_atual, 9, j))
				linka_vertice_com_grupo(grupo_atual, 9, j, vertices);

	}
}

//Linka colunas e linhas juntas
void link_cols_and_rows(struct vertice **vertices)
{
	int linha_atual[9];
	int coluna_atual[9];
	int i = 0;
	int j = 0;
	for (i = 0; i < 9; i++) {
		for (j = 0; j < 9; j++) {
			linha_atual[j] = (i * 9) + j;
			coluna_atual[j] = (j * 9) + i;
		}

		//Procura pelos índices dos vértices e linka-os em um grupo junto
		for (j = 0; j < 81; j++) 
			if (verifica_indice_grupo(linha_atual, 9, j))
				linka_vertice_com_grupo(linha_atual, 9, j, vertices);

		for (j = 0; j < 81; j++) 
			if (verifica_indice_grupo(coluna_atual, 9, j))
				linka_vertice_com_grupo(coluna_atual, 9, j, vertices);
	}
}
		
//Função que cria o grafo, carrega os vértices e faz a sua ligação com o padrão do sudoku
struct grafo *load_initial(int matriz[9][9]){
	
	FILE *fp;
	fp = fopen("mtx", "w");
	
	int indice = 0;
	int i,j;

	char *buffer = malloc(sizeof(char) * (MAX_ELEM_MATRIZ+1));
	char valor;
	
// 	buffer[0] = '\0';

	//Passa os valores da matriz para o vetor temporario e simultaneamente converte o valor inteiro para char
	for(i=0;i<9;i++){
		for(j=0;j<9;j++){
			valor = matriz[i][j]+'0';
			fprintf(fp,"%c",valor);
			indice++;
		}
	}
	fclose(fp);
	
	fp = fopen("mtx", "r");
	
	fgets(buffer, (MAX_ELEM_MATRIZ+1), fp);

	struct grafo *grafo = cria_grafo();
	struct vertice **vertices = malloc(sizeof(void *) * MAX_ELEM_MATRIZ);

	//Cria o grafo com os vértices correspondentes
	for (i = 0; i < MAX_ELEM_MATRIZ; i++) { 
		//Adiciona todas as possibilidade de 9 cores se tiver um 0, que é um campo não preenchido
		if (buffer[i] == '0') {
			vertices[i] = adiciona_vertice(grafo, 9, 0);
		}
		else {
			//Adiciona uma possibilidade única se tiver um campo já preenchido
			vertices[i] = adiciona_vertice(grafo, 1, buffer[i] - '0');
		}
	}
	
	linka_grupos_3(vertices); //OK
	link_cols_and_rows(vertices); //OK

	free(buffer); //OK
	free(vertices); //OK
	fclose(fp);
 	remove("mtx");

	return grafo;
}

void gera_sudoku_aleatorio_com_solucao(struct grafo *grafo, int size, int matriz[9][9]){
	int *saida = malloc(sizeof(int) * (size * size));
	int j=80;
	int k,l;
	k=l=0;
	
	saida=pega_valores(grafo, 9);

	for(k=0;k<9;k++){
		for(l=0;l<9;l++){
			matriz[k][l]=saida[j];
			j--;
		}
	}
	
	/*printf("\n\n");
	for(k=0;k<9;k++){
		for(l=0;l<9;l++){
			printf("%3d",matriz[k][l]);
		}
		printf("\n");
	}*/

	srand(time(NULL));
	int aleatorio, contador, i, o;
	int vetor_usados[valores_iniciais];
	
	contador = 80;
	o = 0;
	
	for(j=0;j<9;j++){
		for(k=0;k<9;k++){
			matriz[j][k] = 0;
		}
	}
	
	for(j=0;j<valores_iniciais;j++)
		vetor_usados[j] = -1;
	
	for(i=0;i<valores_iniciais;i++){
		
		//Gera um número aleatório de 0 a 81
		aleatorio = rand()%(MAX_ELEM_MATRIZ-1);
// 		printf("ALEAT = %d\n", aleatorio);
		
		//Contador para andar no vetor_usados
		o = 0;
		while(1){
			//Caso o aleatório já esteja no vetor_usados gera um novo número
			if (aleatorio == vetor_usados[o]){
				aleatorio = rand()%(MAX_ELEM_MATRIZ-1);
// 				printf("NOVO ALEAT = %d\n", aleatorio);
				//Começa de novo o vetor, verificando se o número gerado já está no vetor
				o = 0;
			}
			else{
				//Caso seja -1, quer dizer que pode colocar no vetor nessa posição, porque não tem nenhum número
				if (vetor_usados[o] == -1)
					break;
				//Caso não seja -1, é porque já tem um valor nessa posição do vetor, então irá para o próximo elemento, de índice o+1
				o++;
			}
			//Caso percorra todos os valores do vetor e tenham todos já sido preenchidos, vai quebrar o while
			if (o == valores_iniciais){
				break;
			}
		}
		
		//Armazena o aleatório gerado na última posição do vetor_usados
		vetor_usados[o] = aleatorio;
		
//  		for(j=0;j<valores_iniciais;j++)
//   			printf("VETOR_USADOS = %d\n", vetor_usados[j]);
		
		//Loop que percorre toda a matriz procurando o valor gerado na vairável aleatorio a ser inserido na matriz
		for(j=0;j<9;j++){
			for(k=0;k<9;k++){
				//Caso ache o valor da variável aleatorio
				if(contador == aleatorio){
					break;
				}
				//Caso não ache, aumenta o contador para achar a posição
				contador-=1;
// 				printf("CONTADOR = %d\n", contador);
			}
			//Caso tenha achado a posição, quebra o for correspondente.
			if (contador == aleatorio){
				//Gera um número de 1 a 9 para ser colocado no sudoku
				matriz[j][k] = saida[contador];
// 				printf("VALOR GERADO = %d\n", matriz[j][k]);
				break;
			}
		}
		//Zera o contador pra começar a contagem para a próxima variável aleatorio gerada.
		contador = 80;
// 		printa_matriz(matriz);
	}
	
	/*printf("\n\n");
	for(k=0;k<9;k++){
		for(l=0;l<9;l++){
			printf("%3d",matriz[k][l]);
		}
		printf("\n");
	}*/

	grafo = load_initial(matriz);
	coloracao_grafo(grafo->vertices, 9);

	if (verifica_coloracao_grafo_completo(grafo))
		printa_sudoku(grafo, 9);
	else
		printf("Não foi possível colorir o grafo para o sudoku.\n");
	
}

void inicializa_quadrantes(Verificacao verificacao[9]){
	
	int i;
	
	//Inicializa quadrantes
	for(i=0;i<9;i++)
		verificacao[i].quadrante = i+1;
}

void gera_possivel_sudoku_aleatorio(int matriz[9][9], Verificacao verificacao[9]){
	
	srand(time(NULL));
	int aleatorio, contador, i, j, k, o, validacao;
	int vetor_usados[valores_iniciais];
	
	inicializa_quadrantes(verificacao);
	
	getchar();
	
	contador = 0;
	o = 0;
	validacao = 1;
	
	for(j=0;j<9;j++){
		for(k=0;k<9;k++){
			matriz[j][k] = 0;
		}
	}
	
	for(j=0;j<valores_iniciais;j++)
		vetor_usados[j] = -1;
	
	for(i=0;i<valores_iniciais;i++){
		
		//Gera um número aleatório de 0 a 81
		aleatorio = rand()%(MAX_ELEM_MATRIZ-1);
// 		printf("ALEAT = %d\n", aleatorio);
		
		//Contador para andar no vetor_usados
		o = 0;
		while(1){
			//Caso o aleatório já esteja no vetor_usados gera um novo número
			if (aleatorio == vetor_usados[o]){
				aleatorio = rand()%(MAX_ELEM_MATRIZ-1);
// 				printf("NOVO ALEAT = %d\n", aleatorio);
				//Começa de novo o vetor, verificando se o número gerado já está no vetor
				o = 0;
			}
			else{
				//Caso seja -1, quer dizer que pode colocar no vetor nessa posição, porque não tem nenhum número
				if (vetor_usados[o] == -1)
					break;
				//Caso não seja -1, é porque já tem um valor nessa posição do vetor, então irá para o próximo elemento, de índice o+1
				o++;
			}
			//Caso percorra todos os valores do vetor e tenham todos já sido preenchidos, vai quebrar o while
			if (o == valores_iniciais){
				break;
			}
		}
		
		//Armazena o aleatório gerado na última posição do vetor_usados
		vetor_usados[o] = aleatorio;
		
//  		for(j=0;j<valores_iniciais;j++)
//   			printf("VETOR_USADOS = %d\n", vetor_usados[j]);
		
		//Loop que percorre toda a matriz procurando o valor gerado na vairável aleatorio a ser inserido na matriz
		for(j=0;j<9;j++){
			for(k=0;k<9;k++){
				//Caso ache o valor da variável aleatorio
				if(contador == aleatorio){
					break;
				}
				//Caso não ache, aumenta o contador para achar a posição
				contador+=1;
// 				printf("CONTADOR = %d\n", contador);
			}
			//Caso tenha achado a posição, quebra o for correspondente.
			if (contador == aleatorio){
				//Gera um número de 1 a 9 para ser colocado no sudoku
				matriz[j][k] = 1+rand()%9;
// 				printf("VALOR GERADO = %d\n", matriz[j][k]);
				break;
			}
		}
		//Zera o contador pra começar a contagem para a próxima variável aleatorio gerada.
		contador = 0;
	}
	
	while(validacao == 1){
		validacao = 0;
		//Verifica números iguais na mesma linha
		for(i=0;i<9;i++){
			for(j=0;j<9;j++){
				if (matriz[i][j] != 0){
					//Percorre linha por linha procurando elementos iguais
					for(k=0;k<9;k++){
						if (matriz[i][j] == matriz[k][j] && k!=i){
							matriz[i][j] = 1+rand()%9;
							k = 0;
							validacao = 1;
						} //Exit if de elem iguais
					} //Exit for da varredura das linhas
				} //Exit if de elemento == 0
			} //Exit for das colunas
		} //Exit for das linhas
		
// 		printa_matriz(matriz);
		
		//Verifica números iguais na mesma coluna
		for(i=0;i<9;i++){
			for(j=0;j<9;j++){
				if (matriz[i][j] != 0){
					//Percorre coluna por coluna procurando elementos iguais
					for(k=0;k<9;k++){
						if (matriz[i][j] == matriz[i][k] && k!=j){
							matriz[i][j] = 1+rand()%9;
							k = 0;
							validacao = 1;
						} //Exit if de elem iguais
					} //Exit for da varredura das colunas
				} //Exit if de elemento == 0
			} //Exit for das colunas
		} //Exit for das linhas
		
		/*contador = 0;
		sair = 0;
		
		//Acha o quadrante do numero
//  		contador = i*9+j;
		for(i=0;i<9;i++){
			for(j=0;j<9;j++){
				if (i == a && j == b){
					sair = 1;
					break;
				}
				contador+=1;
			}
			if (sair == 1)
				break;
		}
		
		contador = 0;
		sair = 0;
		
		//Passa por todos os quadrantes procurando aquele número e compara com o seu quadrante.
		for(j=0;j<9;j++){
			for(k=0;k<9;k++){
				for(o=0;o<9;o++){
					if (verificacao[i].valores[j] == contador)
						if ((matriz[a][b] == matriz[k][o]) && (a!=k) && (b != o)){
							printf("O valor digitado já existe nesse quadrante! Favor, digitar novamente.\n");
							sair = 1;
							break;
						}
				}
				contador += 1;
				if (sair == 1)
					break;
			}
			if (sair == 1)
				break;
		}*/
	}
}

void gera_sudoku_branco(int matriz[9][9]){

	//inicializa a matriz vazia com 0's	
	int j,k;
	for(j=0;j<9;j++){
		for(k=0;k<9;k++){
			matriz[j][k]=0;
		}
	}
}

//Função que mostra inicialmente o sudoku sem tratamento de coloração
void mostra_sudoku(int matriz[9][9]){
	
	int i,j,index=0;
	int dim=83;

	system("clear");
		
	index=1;
		printf("           %-1d      %-1d      %-1d       %-1d      %-1d       %-1d      %-1d      %-1d      %-1d",0,1,2,3,4,5,6,7,8);

	printf("\n");
		printf("        -------------------------------------------------------------------");
	printf("\n");


	for(i=0;i<9;i++){
		printf("%5d %3c",i,'|');
		for(j=0;j<9;j++){
			printf("%3c %3c", matriz[i][j]==0?' ': matriz[i][j]+'0',index==dim-1 ?' ':'|');
			if(index % 3==0)
				printf("%c",'|');
			if(index % 9==0){
				printf("\n");
					printf("        -------------------------------------------------------------------");
			}
			if(index % 27==0){
				printf("\n");
					printf("        -------------------------------------------------------------------");
			}
			index+=1;
		}
		printf("\n");
	}
	printf("\n");
}

int validacao(int linha, int coluna, int valor_requerido, int matriz[9][9], Verificacao verificacao[9]){
	
	int i, j, contador, sair;
	
	//Verifica números iguais na mesma linha
	for(j=0;j<9;j++){
		if (matriz[linha][j] == valor_requerido){
			printf("O valor digitado já existe nessa linha! Favor, digitar novamente.\n");
			printf("Aperte ENTER para continuar");
			getchar();
			return 1;
		} //Exit if de elem iguais
	} //Exit for das colunas
	
	//Verifica números iguais na mesma coluna
	for(i=0;i<9;i++){
		if (matriz[i][coluna] == valor_requerido){
			printf("O valor digitado já existe nessa coluna! Favor, digitar novamente.\n");
			printf("Aperte ENTER para continuar\n");
			getchar();
			return 1;
		}
	} //Exit for das linhas
	
	contador = 0;
	sair = 0;
	
	//Acha o quadrante do numero
	//contador = linha*9+coluna;
	for(i=0;i<9;i++){
		for(j=0;j<9;j++){
			if (i == linha && j == coluna){
				sair = 1;
				break;
			}
			contador+=1;
		}
		if (sair == 1)
			break;
	}
	
	sair = 0;
	
	for(i=0;i<9;i++){
		for(j=0;j<9;j++){
			if (verificacao[i].valores[j].valor == contador){
				sair = 1;
				break;
			}
		}
		if (sair == 1)
			break;
	}
	
	//Passa por todos os quadrantes procurando aquele número e compara com o seu quadrante.
	for(j=0;j<9;j++){
		printf("VALOR DA MATRIZ = %d\n VALOR REQUERIDO = %d\n", matriz[verificacao[i].valores[j].linha][verificacao[i].valores[j].coluna],valor_requerido);
		if (matriz[verificacao[i].valores[j].linha][verificacao[i].valores[j].coluna] == valor_requerido){
			printf("O valor digitado já existe nesse quadrante! Favor, digitar novamente.\n");
			printf("Aperte ENTER para continuar\n");
			getchar();
			return 1;
		}
	}
	
	return 0;
}

void jogar(int matriz[9][9], Verificacao verificacao[9]){
	
	int opcao2, linha, coluna, resultado, valor_requerido;
	
	resultado = 0;
	
	struct grafo *grafo;
	
	mostra_sudoku(matriz);
	while(1){
		printf("(1) Para jogar\n(2) Para mostrar solucao\n(3) Sair\n");
		printf("Escolha uma das opções acima: ");
		scanf("%d",&opcao2);
		
		if (opcao2==1){
			resultado = 0;
			
			printf("Informe para jogar: \n");
			printf("    linha: ");
			scanf("%d", &linha);
			printf("    coluna: ");
			scanf("%d",&coluna);
			
			printf("Qual valor deseja inserir (de 1 a 9)? ");
			scanf("%d", &valor_requerido);
			flush_in();
			while(valor_requerido < 1 || valor_requerido > 9){
				printf("Valor invalido. Digite novamente: ");
				scanf("%d", &valor_requerido);
				flush_in();
			}
			printf("\n");
			
			resultado = validacao(linha, coluna, valor_requerido, matriz, verificacao);
			
			//Caso não tenha número inválido. Se tivesse, o valor de resultado seria 1
			if (resultado == 0)
				matriz[linha][coluna] = valor_requerido;
			
			mostra_sudoku(matriz);
				
		}else if(opcao2==2){
			grafo = load_initial(matriz);
			coloracao_grafo(grafo->vertices, 9);
			if (verifica_coloracao_grafo_completo(grafo))
				printa_sudoku(grafo, 9);
			else
				printf("Não foi possível colorir o grafo para o sudoku.\n");
			deleta_grafo(grafo);
			exit(0);
		}else{
			exit(0);

		}
	}
}

void menu(int matriz[9][9]){
	
	int opcao, opcao2, resultado, linha, coluna, valor_requerido;
	
	//Inicializa verificação de posições
	Verificacao verificacao[9] = {
	       //q, v, l, c, v, l, c, v, l, c, v, l, c, v, l, c, v, l, c, v, l, c, v, l, c, v, l, c
		{0, 0, 0, 0, 1, 0, 1, 2, 0, 2, 9, 1, 0,10, 1, 1,11, 1, 2,18, 2, 0,19, 2, 1,20, 2, 2},
		{1, 3, 0, 3, 4, 0, 4, 5, 0, 5,12, 1, 3,13, 1, 4,14, 1, 5,21, 2, 3,22, 2, 4,23, 2, 5},
		{2, 6, 0, 6, 7, 0, 7, 8, 0, 8,15, 1, 6,16, 1, 7,17, 1, 8,24, 2, 6,25, 2, 7,26, 2, 8},
		{3,27, 3, 0,28, 3, 1,29, 3, 2,36, 4, 0,37, 4, 1,38, 4, 2,45, 5, 0,46, 5, 1,47, 5, 2},
		{4,30, 3, 3,31, 3, 4,32, 3, 5,39, 4, 3,40, 4, 4,41, 4, 5,48, 5, 3,49, 5, 4,50, 5, 5},
		{5,33, 3, 6,34, 3, 7,35, 3, 8,42, 4, 6,43, 4, 7,44, 4, 8,51, 5, 6,52, 5, 7,53, 5, 8},
		{6,54, 6, 0,55, 6, 1,56, 6, 2,63, 7, 0,64, 7, 1,65, 7, 2,72, 8, 0,73, 8, 1,74, 8, 2},
		{7,57, 6, 3,58, 6, 4,59, 6, 5,66, 7, 3,67, 7, 4,68, 7, 5,75, 8, 3,76, 8, 4,77, 8, 5},
		{8,60, 6, 6,61, 6, 7,62, 6, 8,69, 7, 6,70, 7, 7,71, 7, 8,78, 8, 6,79, 8, 7,80, 8, 8}
	};
		
	system("clear");
	
	printf("----------- MENU -------------\n");
	printf("(1) Gerar sudoku aleatorio\n(2) Gerar sudoku em branco\n(3) Sair\n");
	printf("Escolha uma das opções acima: ");
	scanf("%d", &opcao);
	struct grafo *grafo;
	switch (opcao){
		case 1:
			grafo = load_initial(matriz);
			coloracao_grafo(grafo->vertices, 9);
			gera_sudoku_aleatorio_com_solucao(grafo,9,matriz);
			deleta_grafo(grafo);
				
			jogar(matriz, verificacao);
			break;
		case 2:
			gera_sudoku_branco(matriz);
			mostra_sudoku(matriz);
			while(1){
				printf("(1) Para jogar\n(2) Para mostrar solucao\n(3) Sair\n");
				printf("Escolha uma das opções acima: ");
				scanf("%d",&opcao2);
				
				if (opcao2==1){
					resultado = 0;
					
					printf("Informe para jogar: \n");
					printf("    linha: ");
					scanf("%d", &linha);
					printf("    coluna: ");
					scanf("%d",&coluna);
					
					printf("Qual valor deseja inserir (de 1 a 9)? ");
					scanf("%d", &valor_requerido);
					flush_in();
					while(valor_requerido < 1 || valor_requerido > 9){
						printf("Valor invalido. Digite novamente: ");
						scanf("%d", &valor_requerido);
						flush_in();
					}
					printf("\n");
					
					resultado = validacao(linha, coluna, valor_requerido, matriz, verificacao);
					
					//Caso não tenha número inválido. Se tivesse, o valor de resultado seria 1
					if (resultado == 0)
						matriz[linha][coluna] = valor_requerido;
					
					mostra_sudoku(matriz);
						
				}else if(opcao2==2){
					system("clear");
					grafo = load_initial(matriz);
					coloracao_grafo(grafo->vertices, 9);
					if (verifica_coloracao_grafo_completo(grafo))
						printa_sudoku(grafo, 9);
					else
						printf("Não foi possível colorir o grafo para o sudoku.\n");
					deleta_grafo(grafo);
					exit(0);
				}else{
					exit(0);
				}
			}
			break;
		case 3:
			exit(0);
	}
}

int main(int argc, char **argv){
	
	//Inicializacao da matriz de teste
	int matriz[9][9]={{8,7,4,1,5,2,6,9,3},{6,2,9,3,7,8,5,4,1},{5,3,1,9,6,4,7,2,8},{3,5,7,4,1,9,8,6,2},{9,8,2,7,3,6,1,5,4},
			  {1,4,6,8,2,5,9,3,7},{7,6,8,5,4,3,2,1,9},{2,9,3,6,8,1,4,7,5},{4,1,5,2,9,7,3,8,6}};
	
	inicializa_matriz_de_iniciais();
	
	menu(matriz);
	
	return 0;
}