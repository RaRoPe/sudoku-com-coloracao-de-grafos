#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "graph.h"

//Verifica se o vetor tem o valor da variavel valor
bool conteudo_array(int *array, int count, int valor)
{
	int i = 0;
	for (i = 0; i < count; i++)
		if (array[i] == valor)
			return true;
	
	return false;
}


//Verifica se um vértice está ligado a somente vértices de cores diferentes
bool verifica_cor_unica(struct vertice *vertice, int cor){
	
	if (vertice->possivel_numero == 1 && vertice->valor_corrente == cor)
		return true;

	return false;
}


//Determina se as bordas de um vértice não permitiriam que uma determinada cor fosse escolhida
bool troca_de_cor_aresta(struct vertice *vertice, int cor){
	
	bool se_na_aresta = false;
	struct aresta *ultima_aresta = vertice->arestas;

	while (ultima_aresta != NULL) {
		if (ultima_aresta->vizinho_ultima_aresta != NULL) {
			struct vertice *conectado = ultima_aresta->vizinho_ultima_aresta;
			se_na_aresta = verifica_cor_unica(conectado, cor);
		}

		if (se_na_aresta == true)
			return true;

		ultima_aresta = ultima_aresta->prox;
	}

	return se_na_aresta;
}


//Função para verificar a possibilidade de coloração de um nó do grafo, verificando toda a vizinhança e troca cor do nó caso necessário
int verifica_cor_valida(struct vertice *vertice, int *removido, int contador_removido, int cor){
	
	if (vertice->possivel_numero == 1 && vertice->valor_corrente == cor)
		return true;
	else
		if (!conteudo_array(removido, contador_removido, cor))
			return !troca_de_cor_aresta(vertice, cor);
	
	return false;
}


//Algoritmo recursivo, inspirado em backtracking para a coloração do grafo
bool coloracao_grafo(struct vertice *vertice, int qtd_cores){
	
	if (vertice == NULL)
		return true;

	int indice_removido = 0;
	int cores_removidas[qtd_cores];
	
	int valor_original = vertice->valor_corrente;
	int numero_original = vertice->possivel_numero;

	if (numero_original == 1)
		return coloracao_grafo(vertice->prox, qtd_cores);	

	int i = 0;
	for (i = 1; i <= numero_original; i++) {
		if (!verifica_cor_valida(vertice, cores_removidas, indice_removido, i))
			continue;

		vertice->valor_corrente = i;
		vertice->possivel_numero = 1;

		bool sucesso = coloracao_grafo(vertice->prox, qtd_cores);

		if (sucesso == false) {
			cores_removidas[indice_removido] = i;
			indice_removido++;
			vertice->possivel_numero = numero_original;
			vertice->valor_corrente = valor_original;
		}
		else {
			return true;
		}
	}

	return false;
}


//Função que checa se cada um dos vértices somente tem uma possibilidade de cor, correspondendo à coloração completa
bool verifica_coloracao_grafo_completo(struct grafo *grafo){
	
	struct vertice *proximo_vertice = grafo->vertices;
	while (proximo_vertice != NULL) {
		if (proximo_vertice->possivel_numero > 1){
			return false;
		}

		proximo_vertice = proximo_vertice->prox;
	}

	return true;
}

