#include <stdlib.h>
#include <stdio.h>
#include "graph.h"

struct grafo *cria_grafo()
{
	struct grafo *criado = malloc(sizeof(struct grafo));
	criado->vertices = NULL;
	criado->indice_vertice = 1;

	return criado;
}


void deleta_grafo(struct grafo *grafo)
{
	struct vertice *proximo_vertice = grafo->vertices;

	while (proximo_vertice != NULL) {
		struct vertice *atual = proximo_vertice;
		proximo_vertice = proximo_vertice->prox;

		struct aresta *proxima_aresta = atual->arestas;
		while (proxima_aresta != NULL) {
			struct aresta *aresta_atual = proxima_aresta;
			proxima_aresta = proxima_aresta->prox;

			free(aresta_atual);
		}

		free(atual);
	}

	free(grafo);
}


struct vertice *adiciona_vertice(struct grafo *grafo, int possivel_numero, int valor)
{
	struct vertice *prox = malloc(sizeof(struct vertice));
	prox->index = grafo->indice_vertice++;
	prox->valor_corrente = valor;
	prox->possivel_numero = possivel_numero;
	prox->arestas = NULL;
	prox->prox = grafo->vertices;
	grafo->vertices = prox;

	return prox;
}


void adiciona_aresta(struct vertice *vertice1, struct vertice *vertice2)
{
	struct aresta *prox = malloc(sizeof(struct aresta));
	prox->vizinho_ultima_aresta = vertice2;
	prox->prox = vertice1->arestas;
	vertice1->arestas = prox;
}


void printa_grafo(struct grafo *grafo)
{
	int contador_de_arestas = 0;
	struct vertice *ultimo_vertice = grafo->vertices;
	
	while (ultimo_vertice != NULL) {
		printf("Vértice %d[%d] conecta a: ", ultimo_vertice->index, ultimo_vertice->valor_corrente);
		struct aresta *ultima_aresta = ultimo_vertice->arestas;

		while (ultima_aresta != NULL) {
			if (ultima_aresta->vizinho_ultima_aresta != NULL) {
				contador_de_arestas++;
				printf("%d[%d], ", ultima_aresta->vizinho_ultima_aresta->index,
					ultima_aresta->vizinho_ultima_aresta->valor_corrente);
			}

			ultima_aresta = ultima_aresta->prox;
		}

		printf("\n");

		ultimo_vertice = ultimo_vertice->prox;
	}

	printf("Quantidade total de arestas: %d\n", contador_de_arestas);
}


void printa_sudoku(struct grafo *grafo, int size)
{
	int *saida = malloc(sizeof(int) * (size * size));

	struct vertice *ultimo_vertice = grafo->vertices;

	int index = 0;
	while (ultimo_vertice != NULL) {
		saida[index] = ultimo_vertice->valor_corrente;
		
		ultimo_vertice = ultimo_vertice->prox;
		index++;
	}
	
	/* Instrucoes para fazer as malhas do sudoku*/
	printf("\n");
	int i;
	int dim=83;
	index--;
	for(i=0;i<11;i++)
		printf("------");
	printf("\n");
	for (; index >= 0; index--){
		printf("%3d %3c", saida[index], index==dim-1 ?' ':'|');

		
		if(index % 3==0)
			printf("%c",'|');
		if(index % 9==0){
			printf("\n");
			for(i=0;i<11;i++)
				printf("------");
		if(index % 27==0){
			printf("\n");
			for(i=0;i<11;i++)
				printf("------");
		}
		printf("\n");
		}
	}
	printf("\n");

	free(saida);
}


int *pega_valores(struct grafo *grafo, int size){
	int *saida = malloc(sizeof(int) * (size * size));

	struct vertice *ultimo_vertice = grafo->vertices;

	int index = 0;
	while (ultimo_vertice != NULL) {
		saida[index] = ultimo_vertice->valor_corrente;
		
		ultimo_vertice = ultimo_vertice->prox;
		index++;
	}
	return saida;
}