# Sudoku com coloracao de grafos

O jogo Sudoku clássico propõe uma grade 9x9. Essa grade é dividida em 9 subgrades quadradas 3x3, também chamadas de blocos. Serão contempladas, então: uma proposta de configuração inicial, e uma solução. O objetivo do jogo é preencher com dígitos de 1 a 9, sendo que nas linhas e colunas dos blocos não pode haver dígitos repetidos.

Será desenvolvido neste projeto:
1) Modelar um jogo Sudoku 9x9 como um grafo;

2) O jogo deve proporcionar:

   2.1) checar se uma proposta numérica for válida;
   
   2.2) gerar soluções e apresentá-las em tela;
   
   2.3) gerar propostas aleatórias para futuros jogos;
   
3) A solução mostrará os passos da solução;
