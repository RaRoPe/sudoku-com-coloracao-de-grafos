#include <stdbool.h>

struct grafo {
	struct vertice *vertices;
	int indice_vertice;
};


struct vertice {
	int valor_corrente;
	int possivel_numero;
	int index;

	struct aresta *arestas;
	struct vertice *prox;
};


struct aresta {
	struct vertice *vizinho_ultima_aresta;
	struct aresta *prox;
};


struct grafo *cria_grafo();
void deleta_grafo(struct grafo *grafo);

struct vertice *adiciona_vertice(struct grafo *grafo, int possivel_numero, int valor);
void add_aresta(struct vertice *vertice1, struct vertice *vertice2);

void printa_grafo(struct grafo *grafo);
void printa_sudoku(struct grafo *grafo, int size);

bool coloracao_grafo(struct vertice *vertice, int qtd_cores);
bool verifica_coloracao_grafo_completo(struct grafo *grafo);

int *pega_valores(struct grafo *grafo, int size);
